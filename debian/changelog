authres (1.2.0-4) unstable; urgency=medium

  * Team Upload
  * Use dh-sequence-python3
  * Bump Standards-Version
  * Set DPT as Maitainer per new DPT Policy

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck",
      since debhelper now does this.
  * Set upstream metadata fields: Name, Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 17:44:16 +0100

authres (1.2.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-all.

 -- Sandro Tosi <morph@debian.org>  Thu, 21 Apr 2022 19:26:47 -0400

authres (1.2.0-2) unstable; urgency=medium

  * Drop python-authres binary (no longer needed in Testing) (Closes: #936163)
  * Bump standards version to 4.4.1 without further change
  * Bump debhelper-compat to 12 without further change
  * Update public upstream key to be minimal

 -- Scott Kitterman <scott@kitterman.com>  Mon, 14 Oct 2019 17:29:06 -0400

authres (1.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards-version to 4.4.0 without further change
  * Update package descriptions to refer to RFC 8601 vice RFC 7601

 -- Scott Kitterman <scott@kitterman.com>  Sat, 03 Aug 2019 16:03:16 -0400

authres (1.1.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Scott Kitterman ]
  * New upstream release
  * Add autopkgtest to run the doctests
  * Bump standards-version to 4.2.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 30 Oct 2018 00:40:47 -0400

authres (1.1.0-1) unstable; urgency=medium

  * New upstream release
    - Add python{3}-setuptools to build-depends
  * Update debian/watch to check signature and use secure uri
  * Honor nocheck option in debian/rules
  * Include egg meta-information for people that use pip/ez_install

 -- Scott Kitterman <scott@kitterman.com>  Fri, 02 Mar 2018 23:29:09 -0500

authres (1.0.2-1) unstable; urgency=medium

  [ Scott Kitterman ]
  * New upstream release
  * Update debian/copyright
  * Bump standards-version to 4.1.3 without further change

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

 -- Scott Kitterman <scott@kitterman.com>  Fri, 16 Feb 2018 02:52:34 -0500

authres (1.0.1-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Fri, 04 Aug 2017 15:29:02 -0400

authres (1.0.0-1) unstable; urgency=medium

  * New upstream release
  * Run tests for all supported python and python3 versions during build
  * Update debian/watch to only download the tarball
  * Bump standards version to 4.0.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 15 Jul 2017 16:11:44 -0400

authres (0.900-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ SVN-Git Migration ]
  * Migrate packaging to git with git-dpm

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards version to 3.9.8 without further change
  * Bump compat/debhelper requirement to 9 to avoid deprecation warning
  * Update package description to refer to RFC 7601 vice RFC 7001
  * Switch to source format 3.0 (quilt) and remove README.source

 -- Scott Kitterman <scott@kitterman.com>  Sun, 11 Dec 2016 01:44:57 -0500

authres (0.800-2) unstable; urgency=medium

  * Upload to unstable

 -- Scott Kitterman <scott@kitterman.com>  Thu, 23 Apr 2015 19:21:01 -0400

authres (0.800-1) experimental; urgency=medium

  * New upstream release
  * Update debian/copyright
  * Update clean override to account for the .pybuild directory
  * Add dh-python to build-depends

 -- Scott Kitterman <scott@kitterman.com>  Mon, 09 Mar 2015 23:08:33 -0400

authres (0.702-1) experimental; urgency=medium

  * New upstream release
  * Convert to using pybuild and simplify debian/rules
    - Add python-authres/python3-authres.install
  * Bump standards version to 3.9.6 without further change
  * Bump compat to 8 and debhelper version to 8.1 for build arch/indep
    separation

 -- Scott Kitterman <scott@kitterman.com>  Sun, 25 Jan 2015 19:47:09 -0500

authres (0.701-1) unstable; urgency=medium

  * New upstream release
  * Update package descriptions to refer to RFC 7001 instead of RFC 5451
  * Bump standards version to 3.9.5 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 25 Jun 2014 06:37:40 -0400

authres (0.602-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Scott Kitterman ]
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Jul 2013 14:59:49 -0400

authres (0.601-1) unstable; urgency=low

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 27 Apr 2013 01:21:46 -0400

authres (0.600-1) unstable; urgency=low

  * Upload to unstable
  * New upstream release
  * Update vcs field in debian/control to the canonical location

 -- Scott Kitterman <scott@kitterman.com>  Thu, 04 Apr 2013 02:06:54 -0400

authres (0.501-1) experimental; urgency=low

  * New upstream release
  * Bump standards version to 3.9.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 11 Feb 2013 16:41:43 -0500

authres (0.500-1) experimental; urgency=low

  * New upstream release
    - Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Tue, 05 Feb 2013 16:01:13 -0500

authres (0.402-1) unstable; urgency=low

  * New upstream release
    - Drop installation of authres/tests with debian/rules since setup.py is
      fixed to do it now
  * Explicitly run tests at build time (is used to be implicitly via byte
    compilation) and support nocheck option
  * Completely remove build directory in clean

 -- Scott Kitterman <scott@kitterman.com>  Sat, 23 Jun 2012 01:22:43 -0400

authres (0.401-2) unstable; urgency=low

  * Install authres/tests so doctests work with installed pacakges

 -- Scott Kitterman <scott@kitterman.com>  Sun, 13 May 2012 14:10:46 -0400

authres (0.401-1) unstable; urgency=low

  * New upstream release
  * Fix debian watch to work with Launchpad again
  * Bump standards version to 3.9.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 13 Apr 2012 15:17:30 -0400

authres (0.399-1) unstable; urgency=low

  * New upstream release
    - Now supports all registered Authentication Results extensions:
        RFC 5617 DKIM/ADSP
        RFC 6008 DKIM signature identification (header.b)
        RFC 6212 Vouch By Reference (VBR)
    - Update package descriptions to include the changed functionality

 -- Scott Kitterman <scott@kitterman.com>  Thu, 09 Feb 2012 01:20:55 -0500

authres (0.3-1) unstable; urgency=low

  * New upstream release
    - Slightly adjusted function input order
    - Now supports generation of comments
    - Generates and parses (less comment parsing) all RFC 5451 examples

 -- Scott Kitterman <scott@kitterman.com>  Mon, 09 Jan 2012 08:54:49 -0500

authres (0.2-2) unstable; urgency=low

  * No change rebuild for switch to python2.7 as default

 -- Scott Kitterman <scott@kitterman.com>  Tue, 27 Sep 2011 17:23:07 -0400

authres (0.2-1) unstable; urgency=low

  * New upstream release
    - Completely reworked API
    - Update debian/copyright
    - Supports python3
  * Simplify clean rule
  * Add python3-authres package
    - Add to debian/control
    - Add python3-all-dev to build-depends
    - Adjust debian/rules
    - Add debian/python3-authres.docs

 -- Scott Kitterman <scott@kitterman.com>  Thu, 11 Aug 2011 11:04:00 -0400

authres (0.1-2) unstable; urgency=low

  * Protect for loops in debian/rules with set -e
  * Drop ${python:Breaks}, no longer used by dh_python2
  * Bump standards version to 3.9.2 without further change
  * Rebuild for Python transition

 -- Scott Kitterman <scott@kitterman.com>  Fri, 15 Apr 2011 19:32:34 -0400

authres (0.1-1) unstable; urgency=low

  * Initial release (Closes: #618302)

 -- Scott Kitterman <scott@kitterman.com>  Mon, 14 Mar 2011 12:19:42 -0400
